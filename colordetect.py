# -*- coding:utf-8 -*-
import cv2
import numpy as np
import sys
#sys.path.append('/home/zhy/桌面/CubeSolver-master')
import serial
from colordraw import *
import math
import json
import kociemba
import time
from multiprocessing import Pool
from concurrent.futures import ThreadPoolExecutor

kernel_15 = np.ones((15,15),np.uint8)#15x15的卷积核
kernel_50 = np.ones((50,50),np.uint8)#50x50的卷积核
draw = np.zeros((4800,6400, 3), dtype="uint8")#创建一个高4800*宽6400画布
#处理图片
def colorMatch(side):
    
    cube_rgb = cv2.imread( side + '.jpg')
    cube_gray = cv2.cvtColor(cube_rgb, cv2.COLOR_BGR2GRAY)#颜色转换gray
    cube_hsv = cv2.cvtColor(cube_rgb,cv2.COLOR_BGR2HSV)#颜色转换hsv
    cube_gray = cv2.adaptiveThreshold(cube_gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)#自适应滤波
    #cv2.namedWindow(side,cv2.WINDOW_NORMAL)
    #cv2.imshow(side,cube_rgb)
    # 白色
    lower_white = np.array([0, 0, 201])
    upper_white = np.array([180, 50, 255])
    white_mask = cv2.inRange(cube_hsv, lower_white, upper_white)
    white_erosion = cv2.erode(white_mask, kernel_15, iterations = 1)
    white_res = cv2.bitwise_and(cube_rgb, cube_rgb, mask = white_erosion)
    #红色
    '''lower_red = np.array([0,50,50])
    upper_red = np.array([10,255,255])
    red_mask0 = cv2.inRange(cube_hsv, lower_red, upper_red)
    lower_red = np.array([172, 135, 150])
    upper_red = np.array([179, 240, 255])
    red_mask1 = cv2.inRange(cube_hsv, lower_red, upper_red)
    red_mask = red_mask0 + red_mask1
    '''
    lower_red = np.array([170, 110, 145])
    upper_red = np.array([182, 240, 255])
    red_mask = cv2.inRange(cube_hsv, lower_red, upper_red)
    red_erosion = cv2.erode(red_mask, kernel_15, iterations = 1)
    red_res = cv2.bitwise_and(cube_rgb, cube_rgb, mask = red_erosion)
    #橙色
    lower_orange = np.array([3, 115, 195])
    upper_orange = np.array([9, 190, 255])
    orange_mask = cv2.inRange(cube_hsv, lower_orange, upper_orange)
    orange_erosion = cv2.erode(orange_mask, kernel_15, iterations = 1)
    orange_res = cv2.bitwise_and(cube_rgb, cube_rgb, mask = orange_erosion)

    #黄色
    lower_yellow = np.array([20, 125, 142])
    upper_yellow = np.array([34, 243, 255])
    yellow_mask = cv2.inRange(cube_hsv, lower_yellow, upper_yellow)
    yellow_erosion = cv2.erode(yellow_mask, kernel_15, iterations = 1)
    yellow_res = cv2.bitwise_and(cube_rgb, cube_rgb, mask = yellow_erosion)
    #绿色
    lower_green = np.array([68,140,120])
    upper_green = np.array([82,255,245])
    green_mask = cv2.inRange(cube_hsv, lower_green, upper_green)
    green_erosion = cv2.erode(green_mask, kernel_15, iterations = 1)
    green_res = cv2.bitwise_and(cube_rgb, cube_rgb, mask = green_erosion)
    #蓝色
    lower_blue = np.array([95, 123, 109])
    upper_blue = np.array([124, 253, 240])
    blue_mask = cv2.inRange(cube_hsv, lower_blue, upper_blue)
    blue_erosion = cv2.erode(blue_mask, kernel_15, iterations = 1)
    blue_res = cv2.bitwise_and(cube_rgb, cube_rgb, mask = blue_erosion)
    #总掩膜
    mask = red_erosion + green_erosion + yellow_erosion + blue_erosion + orange_erosion + white_erosion
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel_50)#开运算分割色块
    mask = cv2.erode(mask, kernel_50, iterations = 1)
    res = cv2.bitwise_and(cube_hsv, cube_hsv, mask = mask)  
       
    edges = cv2.Canny(mask,50,80,apertureSize=5)
    points = cv2.findNonZero(edges)
    #求得图像最大最小值点
    min = np.amin(points, axis=0)#求每列的最小值，axis 0为列,1为行
    max = np.amax(points, axis=0)#求每列的最大值，axis 0为列,1为行
    #求像素x、y最大最小值
    w, h = cube_gray.shape[::-1]#获取cube_gray大小(宽列数w*高行数h)
    x_max = max[0][0]
    y_max = max[0][1]
    x_min = min[0][0]
    y_min = min[0][1] 
    width = int(x_max - x_min)
    height = int(y_max - y_min)
    #定位中点色块颜色及坐标
    def midpoint(x1,y1,x2,y2):
        x_mid = int((x1 + x2)/2)
        y_mid = h - int(((y1 + y2)/2))
        color = res[y_mid, x_mid]
        return ([int(color[0]), int(color[1]), int(color[2])])
    # midpoint获得每个面九个色块的颜色
    mid_1 = midpoint(x_min, y_max, (x_min + int(width/3)), (y_max - int(height/3)))
    mid_2 = midpoint((x_min + int(width/3)), y_max, (x_min + int(width*2/3)), (y_max - int(height/3)))
    mid_3 = midpoint((x_min + int(width*2/3)), y_max, x_max, (y_max - int(height/3)))
    mid_4 = midpoint(x_min, (y_max - int(height/3)), (x_min + int(width/3)), (y_max - int(height*2/3)))
    mid_5 = midpoint((x_min + int(width/3)), (y_max - int(height/3)), (x_min + int(width*2/3)), (y_max - int(height*2/3)))
    mid_6 = midpoint((x_min + int(width*2/3)), (y_max - int(height/3)), x_max, (y_max - int(height*2/3)))
    mid_7 = midpoint(x_min, (y_max - int(height*2/3)), (x_min + int(width/3)), y_min)
    mid_8 = midpoint(x_min + int(width/3), (y_max - int(height*2/3)), (x_min + int(width*2/3)), y_min)
    mid_9 = midpoint(x_min + int(width*2/3), (y_max - int(height*2/3)), x_max, y_min)
    mids = [mid_1, mid_2, mid_3, mid_4, mid_5, mid_6, mid_7, mid_8, mid_9]


    s=''
    for rgb in mids:#hsv
        if ((0<=rgb[0]<=180 ) and (0<=rgb[1]<=50 ) and( 201<=rgb[2]<=255)):#白
            s+='D'
        elif ((3<=rgb[0]<=9 )and (115<=rgb[1]<=190 )and (195<=rgb[2] <=255)):#澄
            s+='B'
        elif ((95<=rgb[0] <=124) and (123<=rgb[1]<=253) and (109<=rgb[2]<=240)):#蓝
            s+='L'
        elif( (68<=rgb[0]<=82) and (140<=rgb[1]<=255) and (120<=rgb[2]<=245)):#绿
            s+='R'
        elif ((20<=rgb[0]<=34) and (125<=rgb[1]<=243) and(142 <=rgb[2]<=255)):#黄
            s+='U'
        elif ((170<=rgb[0]<=182)and (110<=rgb[1] <=240 )and (145<=rgb[2]<=255 )):#红
            s+='F'
    return(s)

#任务： 1.IO密集型（会有cpu空闲的时间） 
#      2.计算密集型
#多线程对于IO密集型任务有作用，而计算密集型任务不推荐使用多线程。而其中我们还可以得到一个结论：由于GIL锁，多线程不可能真正实现并行，所谓的并行也只是宏观上并行微观上并发，本质上是由于遇到io操作不断的cpu切换所造成并行的现象。由于cpu切换速度极快，所以看起来就像是在同时执行。
#--问题：没有利用多核的优势
#--这就造成了多线程不能同时执行，并且增加了切换的开销，串行的效率可能更高。

def analyzeCube(port):
    time_start=time.time()
    side=['up','right','front','down','left','back']
    #ps=Pool(6)

    #cpu=ps.apply(colorMatch,args=(i,))      # 同步执行
    #up_cpu=ps.apply_async(colorMatch,args=('up',))  # 异步执行
    #right_cpu=ps.apply_async(colorMatch,args=('right',))
    #front_cpu=ps.apply_async(colorMatch,args=('front',))
    #down_cpu=ps.apply_async(colorMatch,args=('down',))
    #left_cpu=ps.apply_async(colorMatch,args=('left',))
    #back_cpu=ps.apply_async(colorMatch,args=('back',))

    # 关闭进程池，停止接受其它进程
    #ps.close()
    # 阻塞进程
    #ps.join()

    #get_str = up_cpu.get() + right_cpu.get() + front_cpu.get() + down_cpu.get() + left_cpu.get() + back_cpu.get()

    #线程池(解决进程池问题)
    ts = ThreadPoolExecutor(6)
    up_cpu=ts.submit(colorMatch,"up")
    down_cpu=ts.submit(colorMatch,"down")
    right_cpu =ts.submit(colorMatch,"right")
    left_cpu =ts.submit(colorMatch,"left")
    front_cpu =ts.submit(colorMatch,"front")
    back_cpu =ts.submit(colorMatch,"back")

    get_str = up_cpu.result() + right_cpu.result() + front_cpu.result() + down_cpu.result() + left_cpu.result() + back_cpu.result()

    print('当前颜色:'+get_str)

    solve_str=kociemba.solve(get_str)
    print(solve_str)
    drawcolor(get_str)

    time_end=time.time()
    print('所需时间:',time_end-time_start,'s')

    #向串口输入指令
    ser = serial.Serial(port, 9600, timeout=None)
    res=ser.write(solve_str.encode("gbk"))
    print("已向串口("+port+")写入"+str(res)+"字节")

    #cv2.waitKey(0)




